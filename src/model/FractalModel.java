// Corentin PACAUD BOEHM / Nicolas TROCHUT / S3B

package model;

public class FractalModel {

	// Variable that define the zoom level of the fractal
	private double zoom = 960 /2.3;

	// Variables that will store the X and Y position of the mouse cursor
	private double mouseX, mouseY;

	// Variables that will store the default "zone" of the fractal (where to look)
	private double x, y;

	// Variables that store the size of the fractal image we will draw
	private int imageX = 1080, imageY = 960;

	// Variable that store the Iteration of the fractal (equals "level" of detail).
	private int maxIteration = 40;

	// Variable that store the number of processors available on the PC
	static private int nbProc = Runtime.getRuntime().availableProcessors()-1;

	/*************************************GETTERS*************************************/

	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZoom() {
		return zoom;
	}
	
	public double getMouseX() {
		return mouseX;
	}
	
	public double getMouseY() {
		return mouseY;
	}
	
	public int getImageX() {
		return imageX;
	}
	
	public int getImageY() {
		return imageY;
	}
	
	public int getMaxIteration() {
		return maxIteration;
	}
	
	public int getnbProc() {
		return nbProc;
	}

	/*********************************************************************************/

	/*************************************SETTERS*************************************/

	public void setX(double x) {
		this.x = x;
	}

	public void setY(double y) {
		this.y = y;
	}

	public void setZoom(double zoom) {
		this.zoom = zoom;
	}

	public void setMouseX(double mouseX) {
		this.mouseX = mouseX;
	}

	public void setMouseY(double mouseY) {
		this.mouseY = mouseY;
	}

	public void setMaxIteration(int maxIteration) {
		this.maxIteration = maxIteration;
	}

	/*********************************************************************************/

	// Put the default values for the zoom (the "camera") and the Iteration variables
	public void resetVariables() {
		zoom = 960 /2.3;
		maxIteration = 40;
	}
}
