// Corentin PACAUD BOEHM / Nicolas TROCHUT / S3B

package controller;

import javafx.scene.image.PixelWriter;
import javafx.scene.paint.Color;
import model.FractalModel;

public class FractalController {

	// Contains all the used variables
	private FractalModel fm = new FractalModel();

	/*************************************GETTERS*************************************/

	public double getX() {
		return fm.getX();
	}

	public double getY() { return fm.getY(); }

	public double getZoom() {
		return fm.getZoom();
	}

	public double getMouseX() {
		return fm.getMouseX();
	}

	public double getMouseY() {
		return fm.getMouseY();
	}

	public int getImageX() {
		return fm.getImageX();
	}

	public int getImageY() {
		return fm.getImageY();
	}

	public int getMaxIteration() {
		return fm.getMaxIteration();
	}

	public int getNbProc() {
		return fm.getnbProc();
	}

	/*********************************************************************************/

	/*************************************SETTERS*************************************/

	public void setX(double x) {
		fm.setX(x);
	}

	public void setY(double y) {
		fm.setY(y);
	}

	public void setZoom(double zoom) {
		fm.setZoom(zoom);
	}

	public void setMouseX(double mouseX) {
		fm.setMouseX(mouseX);
	}

	public void setMouseY(double mouseY) {
		fm.setMouseY(mouseY);
	}

	public void setMaxIteration(int maxIteration) {
		fm.setMaxIteration(maxIteration);
	}

	/*********************************************************************************/

	public void reset() {
		fm.resetVariables();
	}

	/* Main function that do the calculation of the fractal and draw each pixel of the image.
	   It takes the number of Thread, the type of fractal ("MANDELBROT" or "JULIA") and the
	   PixelWriter. The pixelwriter is extracted from a writableImage where we draw the fractal.
	   This way we can drawn the pixel of the (x,y) position with a specific color.
	   This calculation is multi-threaded. It uses the number of available thread to render
	   multiples lines/column at the same time. The calculation is the same with the two types
	   of fractal, but somes variables change. if the pixel is in the fractal, it will draw a
	   black pixel but if it isn't in the fractal, it will use a gradiant color using the hsb
	   function
	   Source : http://sdz.tdct.org/sdz/dessiner-la-fractale-de-mandelbrot.html */
	public void calculFractal(int num, String type, String fractalColor, PixelWriter pw) {
		double c_r, c_i, z_r, z_i, i;
		for (int l = num; l < getImageY(); l+=getNbProc()) {
			for (int c = 0; c < getImageX(); c++) {

				if (type.equals("MANDELBROT")) {
					c_r = c / getZoom() + getX();
					c_i = l / getZoom() + getY();
					z_r = 0;
					z_i = 0;
					i = 0;
				} else {
					c_r =0.285;
					c_i = 0.01;
					z_r = (c / getZoom() +getX());
					z_i =(l / getZoom() +getY());
					i = 0;
				}
				do {
					double tmp = z_r;
					z_r = z_r * z_r - z_i * z_i + c_r;
					z_i = 2 * z_i * tmp + c_i;
					i++;
				} while (z_r * z_r + z_i * z_i < 4 && i < getMaxIteration());

				if (i == getMaxIteration()) {
					pw.setColor(c, l, Color.BLACK);
				}
				else {
					switch (fractalColor) {
						case "COLOR 1":
							pw.setColor(c, l, Color.hsb(i*255/getMaxIteration(),1 ,1 ));
							break;
						case "COLOR 2":
							pw.setColor(c, l, Color.hsb(i*124/124,1 ,1 ));
							break;
						case "COLOR 3":
							pw.setColor(c, l, Color.hsb(i*124/2,1 ,1 ));
							break;
						case "COLOR 4":
							pw.setColor(c, l, Color.hsb(i*255/2,1 ,1 ));
							break;
						case "COLOR 5":
							pw.setColor(c, l, Color.hsb(i*255,1 ,1 ));
							break;
						default:
							pw.setColor(c, l, Color.hsb(i*255/getMaxIteration(),1 ,1 ));
					}
				}
			}
		}
	}
}
