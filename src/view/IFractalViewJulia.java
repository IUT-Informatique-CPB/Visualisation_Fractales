// Corentin PACAUD BOEHM / Nicolas TROCHUT / S3B

package view;

public class IFractalViewJulia extends IFractalView {

    //The constructor will initialise the variables needed to draw the JULIA fractal.
    public IFractalViewJulia() {
        fractalType = "JULIA";
        initJulia();
    }

    public static void main(String[] args) {
        launch(args);
    }
}