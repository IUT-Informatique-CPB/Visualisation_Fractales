// Corentin PACAUD BOEHM / Nicolas TROCHUT / S3B

package view;

public class IFractalViewMandelbrot extends IFractalView {

    //The constructor will initialise the variables needed to draw the MANDELBROT fractal.
    public IFractalViewMandelbrot() {
        fractalType = "MANDELBROT";
        initMandelbrot();
    }

    public static void main(String[] args) {
        launch(args);
    }
}