// Corentin PACAUD BOEHM / Nicolas TROCHUT / S3B

package view;

import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;

// This interface contains all the functions related to the users controls of the application (buttons, labels, drag and drop, mouse-wheel-zoom...)
// They will be described in the IFractalView class of the project.
public interface IFractal {
	public void setOnScroll(Scene scene);
	public void setOnMousePressed(Scene scene);
	public void setOnMouseDragged(Scene scene);
	public void setOnMouseReleased(Scene scene);
	public void addButtons();
	public void addLabels();
	public void updateLabels();
}