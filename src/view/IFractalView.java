// Corentin PACAUD BOEHM / Nicolas TROCHUT / S3B

package view;

import java.util.Vector;

import controller.FractalController;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;

// Abstract class that override functions of the IFractal interface.
public abstract class IFractalView extends Application implements IFractal {

    // Controller used to do the calculation of the fractal and to access the variables.
    private FractalController  fc = new FractalController();

    // Image used to draw the fractal on.
    private WritableImage writableImage = new WritableImage(fc.getImageX(), fc.getImageY());

    // Variable used to print the image on the scene.
    private ImageView imageView = new ImageView(writableImage);

    // "Folder" that regroups all the objects that we see on the window (Fractal image, buttons, labels,...).
    private Group root = new Group();

    // Tool used to draw a pixel with a (x,y) position with a particular color.
    private PixelWriter pixelWriter = writableImage.getPixelWriter();

    // Vector that contains all the threads used in the calculation.
    private Vector<Thrd> threads = new Vector<>();

    // Variable that stores the text which represent the number of iteration of the fractal.
    private Label iterationsNumber;

    // String that represent the type of fractal (Julia or Mandelbrot).
    String fractalType;

    // String that represent the color of the fractal.
    private String fractalColor = "COLOR 1";

    // Function that run a new window, add a title and all the objets we will draw.
    public void start(Stage primaryStage) {

        Scene scene = new Scene(root, fc.getImageX(), fc.getImageY());

        addButtons();

        addLabels();

        calculThrd();

        setOnScroll(scene);

        setOnMousePressed(scene);

        setOnMouseDragged(scene);

        setOnMouseReleased(scene);

        primaryStage.setTitle("Visualisation Fractale (Nicolas Trochut / Corentin Pacaud-Boehm)");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    // Function that reframe the zone where we look for the Julia fractal.
    void initJulia() {
        fc.setY(-1.15);
        fc.setX(-1.45);
    }

    // Function that reframe the zone where we look for the Mandelbrot fractal.
    void initMandelbrot() {
        fc.setX(-2.1);
        fc.setY(-1.2);
    }

    // Function that create all the threads necessary to draw the fractal and launch the calculation
    // Source : Simon Colin
    private void calculThrd() {
        threads.clear();
        for (int i = 0; i < fc.getNbProc(); i++) {
            Thrd t = new Thrd(pixelWriter,i);
            threads.add(t);
            t.start();
        }
        for (Thrd t : threads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                System.err.println("Erreur join threads");
            }
        }
    }

    // Override of the interface function that will zoom in or out on the upper left corner of the fractal when the user scroll the mouse-wheel.
    // Source : Simon Colin
    @Override
    public void setOnScroll(Scene scene) {
        scene.setOnScroll(e->{
            if (e.getDeltaY() > 0) {
                fc.setZoom(fc.getZoom()*1.1);
                fc.setMaxIteration(fc.getMaxIteration()+2);
            } else {
                fc.setZoom(fc.getZoom()*1/1.1);
                fc.setMaxIteration(fc.getMaxIteration()-2);
            }
            updateLabels();
            calculThrd();
        });
    }

    // Override of the interface function that set the position of the mouse cursor when the user click on the screen.
    @Override
    public void setOnMousePressed(Scene scene) {
        scene.setOnMousePressed(e->{
            fc.setMouseX(e.getX());
            fc.setMouseY(e.getY());
        });
    }

    // Override of the interface function that move the image where the mouse cursor go.
    // Source : Simon Colin
    @Override
    public void setOnMouseDragged(Scene scene) {
        scene.setOnMouseDragged(e->{
            imageView.setTranslateX(e.getX()-fc.getMouseX());
            imageView.setTranslateY(e.getY()-fc.getMouseY());
        });
    }

    // Override of the interface function that set the zone and image position and launch the calculation.
    // Source : Simon Colin
    @Override
    public void setOnMouseReleased(Scene scene) {
        scene.setOnMouseReleased(e->{
            fc.setX(fc.getX()-(imageView.getTranslateX()/fc.getZoom()));
            fc.setY(fc.getY()-(imageView.getTranslateY()/fc.getZoom()));
            imageView.setTranslateX(0);
            imageView.setTranslateY(0);
            calculThrd();
        });
    }

    // Override of the interface function that add all the buttons of the interface and the 2 comboBox that are used to select the type and colors of the fractal.
    @Override
    public void addButtons() {
        ObservableList<String> type =
                FXCollections.observableArrayList(
                        "MANDELBROT",
                        "JULIA"
                );

        ObservableList<String> colors =
                FXCollections.observableArrayList(
                        "COLOR 1",
                        "COLOR 2",
                        "COLOR 3",
                        "COLOR 4",
                        "COLOR 5"
                );

        ComboBox<String> fractalType = new ComboBox<>(type);

        ComboBox<String> fractalColor = new ComboBox<>(colors);

        fractalColor.setLayoutX(20);
        fractalColor.setLayoutY(800);
        fractalColor.getSelectionModel().select(colors.get(0));

        fractalType.setLayoutX(20);
        fractalType.setLayoutY(850);
        fractalType.getSelectionModel().select(type.get(0));

        Button calcul = new Button("Calculer");
        calcul.setLayoutX(60);
        calcul.setLayoutY(900);

        Button zoomIn = new Button("+");
        zoomIn.setLayoutX(1000);
        zoomIn.setLayoutY(20);

        Button zoomOut = new Button("-");
        zoomOut.setLayoutX(1040);
        zoomOut.setLayoutY(20);

        Button reset = new Button("Reinitialiser");
        reset.setLayoutX(980);
        reset.setLayoutY(70);

        reset.setOnAction(e->{
            fc.reset();
            if (this.fractalType.equals("JULIA")) {
                initJulia();
            } else {
                initMandelbrot();
            }
            calculThrd();
        });

        zoomIn.setOnAction(e->{
            fc.setZoom(fc.getZoom()*1.1);
            fc.setMaxIteration(fc.getMaxIteration()+2);
            calculThrd();
            updateLabels();
        });

        zoomOut.setOnAction(e->{
            fc.setZoom(fc.getZoom()*1/1.1);
            fc.setMaxIteration(fc.getMaxIteration()-2);
            calculThrd();
            updateLabels();
        });

        calcul.setOnAction(e->{
            this.fractalType = fractalType.getValue();
            this.fractalColor = fractalColor.getValue();
            if (this.fractalType.equals("JULIA")) {
                initJulia();
            }
            else {
                initMandelbrot();
            }
            calculThrd();
        });

        root.getChildren().add(imageView);
        root.getChildren().add(fractalType);
        root.getChildren().add(fractalColor);
        root.getChildren().add(zoomIn);
        root.getChildren().add(zoomOut);
        root.getChildren().add(calcul);
        root.getChildren().add(reset);
    }

    // Override function of the interface that add the labels on the interface
    @Override
    public void addLabels() {
        iterationsNumber = new Label("Iteration : "+fc.getMaxIteration());
        iterationsNumber.setLayoutX(20);
        iterationsNumber.setLayoutY(20);
        root.getChildren().add(iterationsNumber);
    }

    // Override function of the interface that update the labels of the interface
    @Override
    public void updateLabels() {
        iterationsNumber.setText("Iteration :"+fc.getMaxIteration());
    }

    // This class is used to override the run method of the "Thread" class in order to launch the function we want when we launch a thread.
    // Source : Simon Colin
    class Thrd extends Thread {

        private PixelWriter pixelWriter;
        private int num;

        private Thrd(PixelWriter pixelWriter, int num) {
            this.pixelWriter = pixelWriter;
            this.num = num;
        }
        @Override
        public void run() {
            fc.calculFractal(num, fractalType, fractalColor, pixelWriter);
        }

    }
}
